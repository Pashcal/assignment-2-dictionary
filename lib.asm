
section .text
 
 
; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov rax, 60
    syscall

; Принимает указатель rdi на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    xor rax, rax
    
    jmp .loop
    
    .content_loop:
        inc rax
    .loop:
        cmp byte [rdi+rax], 0
        jne .content_loop
    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    mov rsi, rdi ; string pointer
    call string_length
    mov rdx, rax ; strlen
    mov rax, 1 ; write
    mov rdi, 1 ; stdout

    syscall

    ret

; Принимает код символа rdi и выводит его в stdout
global print_char
print_char:
    push rdi

    mov rax, 1 ; write
    mov rsi, rsp ; string pointer
    mov rdi, 1 ; stdout
    mov rdx, 1 ; strlen
    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov rdi, 10
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число rdi в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
    mov rax, rdi
    mov r8, 10

    mov r9, rsp
    dec rsp ; string-end
    mov byte[rsp], 0 ; null-terminator

    .loop:
        dec rsp
        xor rdx, rdx
        div r8
        add rdx, '0'
        mov byte[rsp], dl

        cmp rax, 0
        jg .loop

.ans:
    mov rdi, rsp
    call print_string
    mov rsp, r9
        
    ret

; Выводит знаковое 8-байтовое число rdi в десятичном формате 
global print_int
print_int:
    cmp rdi, 0
    jge .print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

.print_uint:
    call print_uint

    ret

; Принимает два указателя на нуль-терминированные строки rdi rsi, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    mov rax, 1
    xor rcx, rcx
    .compare_char:
        mov bl, byte[rdi+rcx]
        mov dl, byte[rsi+rcx]
        cmp bl, dl
        jne .not_equal
    .check_null:
        test bl, bl
        je .end
        inc rcx
        jmp .compare_char

.not_equal:
    xor rax, rax

.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    xor rax, rax ; sys_read
    xor rdi, rdi ; stdin
    dec rsp      ; string pointer
    mov rsi, rsp
    mov rdx, 1   ; string length
    syscall
    test rax, rax
    jz .EOF
    mov al, [rsp]
    jmp .END
    .EOF:
    xor rax, rax
    .END:
    inc rsp
    ret


; Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
    push r8
    push r9
    push r10
    mov r8, rdi ; buffer pointer
    mov r9, rsi ; buffer length
    dec r9      ; string length

    .skip:
        call read_char
        cmp al, 0
        je .fail
        cmp al, `\t`
        je .skip
        cmp al, `\n`
        je .skip
        cmp al, ' '
        je .skip
        xor r10, r10
        jmp .word

    .word:
        mov byte [r8+r10], al
        inc r10
        cmp r10, r9
        jg .fail
        call read_char
        cmp al, 0
        je .ok
        cmp al, `\t`
        je .ok
        cmp al, `\n`
        je .ok
        cmp al, ' '
        je .ok
        jmp .word

.fail:
    mov byte [r8], 0
    xor r8, r8         ; 0 to rax as pointer
    xor r10, r10       ; 0 to rdx as length
    jmp .exit

.ok:
    mov byte [r8+r10], 0
.exit:
    mov rdx, r10
    mov rax, r8
    pop r10
    pop r9
    pop r8
    ret


global read_string
read_string:
    push r8
    push r9
    push r10
    mov r8, rdi ; buffer pointer
    mov r9, rsi ; buffer length
    dec r9      ; string length

    .skip:
        call read_char
        cmp al, 0
        je .skip
        cmp al, `\t`
        je .skip
        cmp al, `\n`
        je .skip
        xor r10, r10
        jmp .word

    .word:
        mov byte [r8+r10], al
        inc r10
        cmp r10, r9
        jg .fail
        call read_char
        cmp al, 0
        je .ok
        cmp al, `\t`
        je .ok
        cmp al, `\n`
        je .ok
        jmp .word

.fail:
    mov byte [r8], 0
    xor r8, r8         ; 0 to rax as pointer
    xor r10, r10       ; 0 to rdx as length
    jmp .exit

.ok:
    mov byte [r8+r10], 0
.exit:
    mov rdx, r10
    mov rax, r8
    pop r10
    pop r9
    pop r8
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    push r8
    push r9

    xor rax, rax
    xor rcx, rcx
    mov r8, 10

.loop:
    mov r9, [rdi+rcx]
    and r9, 0xff
    cmp r9, '0'
    jl .exit
    cmp r9, '9'
    jg .exit
    
    mul r8
    add rax, r9
    sub rax, '0'
    inc rcx
    jmp .loop

.exit:
    mov rdx, rcx
    pop r9
    pop r8
    ret

; Принимает указатель на строку rdi, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    push r8
    xor r8, r8
    cmp byte [rdi], '-'
    jne .parse_uint
    inc rdi
    mov r8, 1

.parse_uint:
    call parse_uint
    test r8, r8
    jz .exit
    inc rdx
    neg rax
.exit:
    pop r8
    ret 

; Принимает указатель на строку rdi, указатель на буфер rsi и длину буфера rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    xor rcx, rcx
    dec rdx
.loop:
    mov al, byte [rdi+rcx]
    mov byte [rsi+rcx], al
    cmp byte [rdi+rcx], 0
    je .exit
    inc rcx
    cmp rcx, rdx
    jg .error
    jmp .loop

.error:
    mov byte [rsi], 0
    xor rcx, rcx
    jmp .exit
.exit:
    mov rax, rcx
    ret
