%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define buf_size 256            ; full str size
%define str_size 255            ; depends on buf_size, str size without 0 at the end
%define pointer_size 8
%define buf_overflow_code 1
%define key_not_found_code 2

section .rodata
key_not_found_message:
	db "Invalid key", 0
overflow_message:
	db "String length should be not greater than 255", 0

section .bss
buffer: resb buf_size

section .text

global _start
_start:
    mov rdi, buffer
    mov rsi, str_size
    call read_string
    test rax, rax
    je .buffer_overflow

    mov rdi, rax          ; rax is pointer to KEY
    mov rsi, first_word   ; LinkedList first element
    call dict_get         ; get VALUE, where KEY = rax
    test rax, rax
    je .not_found

    add rax, pointer_size ; skip next-element pointer
    mov rdi, rax          ; rax (and rdi) is pointer to VALUE
    push rdi
    call string_length
    inc rax
    pop rdi
    add rdi, rax          ; skip KEY 
    call print_string
    call print_newline

    xor rdi, rdi
    jmp .exit

.buffer_overflow:
    mov rdi, overflow_message
    call print_string
    call print_newline

    mov rdi, buf_overflow_code

    jmp .exit
.not_found:
    mov rdi, key_not_found_message
    call print_string
    call print_newline

    mov rdi, key_not_found_code
.exit:
    jmp exit
