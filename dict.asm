%include "lib.inc"
%define pointer_size 8
%define equals 1

section .text

; rdi is pointer to KEY
; rsi is LinkedList first element
global dict_get
dict_get:
    .loop:
        test rsi, rsi
        je .exit
        mov r12, rdi
        mov r13, rsi
        add rsi, pointer_size
        call string_equals
        mov rdi, r12
        mov rsi, r13
        cmp rax, equals
        je .found
        mov rsi, [rsi]
        jmp .loop
    .found:
        mov rax, rsi
    .exit:
        ret
